﻿using GTA;
using GTA.Math;
using NativeUI;
using System;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;

namespace WarehouseRobbery
{
    public class WarehouseRobbery : Script
    {
        private UIMenu mWarehouse = new UIMenu("Warehouse", "~b~Military Base");
        private MenuPool mPool = new MenuPool();

        //data varriables
        private string _asrifle, _carfile, _sbbomb, _sgbomb;

        private string _asdescr, _cadescr, _sbdescr, _sgdescr;

        private string _wrtitle, _wrammo, _wrgrenades, _wrwaitmessage, _wrammolimitmessage, _wrgrenadeslimitmessage;

        private string _keyMenu, _keyRemoveWeapons;

        private Vector3 baseCoords = new Vector3(-2293.797f, 3191.773f, 32.81007f);

        private bool playerinzone = false;

        public WarehouseRobbery()
        {
            try
            {
                getDataFromXml();
                mPool.Add(mWarehouse);
                addMenuItems();
                mWarehouse.OnItemSelect += MWarehouse_OnItemSelect;
                WarehouseAmmunation.Ammo += 100000000;
                WarehouseAmmunation.Grenades += 100000000;
                KeyDown += WarehouseRobbery_KeyDown;
                Tick += WarehouseRobbery_Tick;
            }
            catch (Exception ex)
            {
                UI.Notify(ex.Message);
            }
        }

        private void addMenuItems()
        {
            mWarehouse.AddItem(new UIMenuItem(_asrifle, _asdescr));
            mWarehouse.AddItem(new UIMenuItem(_carfile, _cadescr));
            mWarehouse.AddItem(new UIMenuItem("Pistol 50", "Get Pistol 50"));
            mWarehouse.AddItem(new UIMenuItem("Bullpup Rifle", "Get Bullpup Rifle"));
            mWarehouse.AddItem(new UIMenuItem("Pistol", "Get Pistol"));
            mWarehouse.AddItem(new UIMenuItem("Sniper Rifle", "Get Sniper Rifle"));
            mWarehouse.AddItem(new UIMenuItem(_sbbomb, _sbdescr));
            mWarehouse.AddItem(new UIMenuItem(_sgbomb, _sgdescr));
            mWarehouse.AddItem(new UIMenuItem("Flare", "Get flare"));
            mWarehouse.AddItem(new UIMenuItem("Grenade", "Get grenade"));
        }

        private void getDataFromXml()
        {
            XDocument wrsettings = XDocument.Load("scripts\\wrsettings.xml");
            try
            {
                var writems = from wrItems in wrsettings.Descendants("wrItems")
                              select new
                              {
                                  item_assaultrifle = wrItems.Element("wrItem_AssaultRifle").Value,
                                  item_carbinerifle = wrItems.Element("wrItem_CarbineRifle").Value,
                                  item_stickybomb = wrItems.Element("wrItem_StickyBomb").Value,
                                  item_smokegrenade = wrItems.Element("wrItem_SmokeGrenade").Value
                              };

                foreach (var warehouseitems in writems)
                {
                    _asrifle = warehouseitems.item_assaultrifle;
                    _carfile = warehouseitems.item_carbinerifle;
                    _sbbomb = warehouseitems.item_stickybomb;
                    _sgbomb = warehouseitems.item_smokegrenade;
                }
            }
            catch (Exception ex)
            {
                UI.Notify(ex.Message);
            }

            try
            {
                var wrdescrs = from wrDescriptions in wrsettings.Descendants("wrDescriptions")
                               select new
                               {
                                   description_assaultrifle = wrDescriptions.Element("wrDescription_AssaultRifle").Value,
                                   description_carbinerifle = wrDescriptions.Element("wrDescription_CarbineRifle").Value,
                                   description_stickybomb = wrDescriptions.Element("wrDescription_StickyBomb").Value,
                                   description_smokegrenade = wrDescriptions.Element("wrDescription_SmokeGrenade").Value
                               };

                foreach (var warehousedescriptions in wrdescrs)
                {
                    _asdescr = warehousedescriptions.description_assaultrifle;
                    _cadescr = warehousedescriptions.description_carbinerifle;
                    _sbdescr = warehousedescriptions.description_stickybomb;
                    _sgdescr = warehousedescriptions.description_smokegrenade;
                }
            }
            catch (Exception ex)
            {
                UI.Notify(ex.Message);
            }

            try
            {
                var wrguis = from wrGui in wrsettings.Descendants("wrGui")
                             select new
                             {
                                 wrGui_Title = wrGui.Element("wrGui_Title").Value,
                                 wrGui_Ammo = wrGui.Element("wrGui_Ammo").Value,
                                 wrGui_Grenades = wrGui.Element("wrGui_Grenades").Value,
                                 wrGui_WaitMessage = wrGui.Element("wrGui_WaitMessage").Value,
                                 wrGui_NotEnoughAmmoMessage = wrGui.Element("wrGui_NotEnoughAmmoMessage").Value,
                                 wrGui_NotEnoughGrenadesMessage = wrGui.Element("wrGui_NotEnoughGrenadesMessage").Value
                             };

                foreach (var wrgui_item in wrguis)
                {
                    _wrtitle = wrgui_item.wrGui_Title;
                    _wrammo = wrgui_item.wrGui_Ammo;
                    _wrgrenades = wrgui_item.wrGui_Grenades;
                    _wrwaitmessage = wrgui_item.wrGui_WaitMessage;
                    _wrammolimitmessage = wrgui_item.wrGui_NotEnoughAmmoMessage;
                    _wrgrenadeslimitmessage = wrgui_item.wrGui_NotEnoughGrenadesMessage;
                }
            }
            catch (Exception ex)
            {
                UI.Notify(ex.Message);
            }

            try
            {
                var wrkeys = from wrKeys in wrsettings.Descendants("wrKeys")
                             select new
                             {
                                 keyMenu = wrKeys.Element("keyMenu").Value,
                                 keyRemoveWeapons = wrKeys.Element("keyRemoveWeapons").Value
                             };

                foreach (var keyItem in wrkeys)
                {
                    _keyMenu = keyItem.keyMenu;
                    _keyRemoveWeapons = keyItem.keyRemoveWeapons;
                }
            }
            catch (Exception ex)
            {
                UI.Notify(ex.Message);
            }
        }

        private void CheckPlayerInZone()
        {
            try
            {
                double zd = new double();
                zd = World.GetDistance(Game.Player.Character.Position, baseCoords);
                if (zd < 2)
                {
                    playerinzone = true;
                }
                else
                {
                    playerinzone = false;
                    if (mWarehouse.Visible == true) { mWarehouse.Visible = false; }
                }
            }
            catch (Exception ex)
            {
                UI.Notify(ex.Message);
            }
        }

        private void WarehouseRobbery_Tick(object sender, EventArgs e)
        {
            mPool.ProcessMenus();
            CheckPlayerInZone();
            mWarehouse.Subtitle.Caption = "~b~" + _wrammo + ": " + /* WarehouseAmmunation.Ammo */"Unlimited" + " | " + _wrgrenades + ": " + /* WarehouseAmmunation.Grenades */ "Unlimited";
            mWarehouse.Title.Caption = _wrtitle;
        }

        private void MWarehouse_OnItemSelect(UIMenu sender, UIMenuItem selectedItem, int index)
        {
            if (selectedItem.Text == _asrifle)
            {
                if (WarehouseAmmunation.Ammo < 150)
                {
                    UI.Notify(_wrammolimitmessage);
                }
                else
                {
                    Game.Player.Character.Weapons.Give(GTA.Native.WeaponHash.AssaultRifle, 30, true, false);
                    Game.Player.Character.Weapons.Current.Ammo += 120;
                    UI.Notify(_wrwaitmessage);
                    //WarehouseAmmunation.Ammo -= 150;
                    Script.Wait(100);
                    Game.Player.WantedLevel = 5;
                }
            }
            else if (selectedItem.Text == _carfile)
            {
                if (WarehouseAmmunation.Ammo < 150)
                {
                    UI.Notify(_wrammolimitmessage);
                }
                else
                {
                    Game.Player.Character.Weapons.Give(GTA.Native.WeaponHash.CarbineRifle, 30, true, false);
                    Game.Player.Character.Weapons.Current.Ammo += 120;
                    UI.Notify(_wrwaitmessage);
                    //WarehouseAmmunation.Ammo -= 150;
                    Script.Wait(100);
                    Game.Player.WantedLevel = 5;
                }
            }
            else if (selectedItem.Text == _sbbomb)
            {
                if (WarehouseAmmunation.Grenades < 1)
                {
                    UI.Notify(_wrgrenadeslimitmessage);
                }
                else
                {
                    Game.Player.Character.Weapons.Give(GTA.Native.WeaponHash.StickyBomb, 0, false, false).Ammo += 1;
                    UI.Notify(_wrwaitmessage);
                    //WarehouseAmmunation.Grenades -= 1;
                    Script.Wait(100);
                    Game.Player.WantedLevel = 5;
                }
            }
            else if (selectedItem.Text == _sgbomb)
            {
                if (WarehouseAmmunation.Grenades < 1)
                {
                    UI.Notify(_wrgrenadeslimitmessage);
                }
                else
                {
                    Game.Player.Character.Weapons.Give(GTA.Native.WeaponHash.SmokeGrenade, 0, false, false).Ammo += 1;
                    UI.Notify(_wrwaitmessage);
                    //WarehouseAmmunation.Grenades -= 1;
                    Script.Wait(100);
                    Game.Player.WantedLevel = 5;
                }
            }
            else if (selectedItem.Text == "Flare")
            {
                if (WarehouseAmmunation.Grenades < 1)
                {
                    UI.Notify(_wrgrenadeslimitmessage);
                }
                else
                {
                    Game.Player.Character.Weapons.Give(GTA.Native.WeaponHash.Flare, 0, false, false).Ammo += 1;
                    UI.Notify(_wrwaitmessage);
                    //WarehouseAmmunation.Grenades -= 1;
                    Script.Wait(100);
                    Game.Player.WantedLevel = 5;
                }
            }
            else if (selectedItem.Text == "Bullpup Rifle")
            {
                if (WarehouseAmmunation.Ammo < 150)
                {
                    UI.Notify(_wrammolimitmessage);
                }
                else
                {
                    Game.Player.Character.Weapons.Give(GTA.Native.WeaponHash.BullpupRifle, 30, true, false);
                    Game.Player.Character.Weapons.Current.Ammo += 120;
                    UI.Notify(_wrwaitmessage);
                    //WarehouseAmmunation.Ammo -= 150;
                    Script.Wait(100);
                    Game.Player.WantedLevel = 5;
                }
            }
            else if (selectedItem.Text == "Pistol 50")
            {
                if (WarehouseAmmunation.Ammo < 150)
                {
                    UI.Notify(_wrammolimitmessage);
                }
                else
                {
                    Game.Player.Character.Weapons.Give(GTA.Native.WeaponHash.Pistol50, 30, true, false);
                    Game.Player.Character.Weapons.Current.Ammo += 120;
                    UI.Notify(_wrwaitmessage);
                    //WarehouseAmmunation.Ammo -= 150;
                    Script.Wait(100);
                    Game.Player.WantedLevel = 5;
                }
            }
            else if (selectedItem.Text == "Grenade")
            {
                if (WarehouseAmmunation.Grenades < 1)
                {
                    UI.Notify(_wrgrenadeslimitmessage);
                }
                else
                {
                    Game.Player.Character.Weapons.Give(GTA.Native.WeaponHash.Grenade, 0, false, false).Ammo += 1;
                    UI.Notify(_wrwaitmessage);
                    //WarehouseAmmunation.Grenades -= 1;
                    Script.Wait(100);
                    Game.Player.WantedLevel = 5;
                }
            }
            else if (selectedItem.Text == "Sniper Rifle")
            {
                if (WarehouseAmmunation.Ammo < 150)
                {
                    UI.Notify(_wrammolimitmessage);
                }
                else
                {
                    Game.Player.Character.Weapons.Give(GTA.Native.WeaponHash.SniperRifle, 30, true, false);
                    Game.Player.Character.Weapons.Current.Ammo += 120;
                    UI.Notify(_wrwaitmessage);
                    //WarehouseAmmunation.Ammo -= 150;
                    Script.Wait(100);
                    Game.Player.WantedLevel = 5;
                }
            }
            else if (selectedItem.Text == "Pistol")
            {
                if (WarehouseAmmunation.Ammo < 150)
                {
                    UI.Notify(_wrammolimitmessage);
                }
                else
                {
                    Game.Player.Character.Weapons.Give(GTA.Native.WeaponHash.Pistol, 30, true, false);
                    Game.Player.Character.Weapons.Current.Ammo += 120;
                    UI.Notify(_wrwaitmessage);
                    //WarehouseAmmunation.Ammo -= 150;
                    Script.Wait(100);
                    Game.Player.WantedLevel = 5;
                }
            }
        }

        public static Keys ConvertFromString(string keystr)
        {
            try
            {
                return (Keys)Enum.Parse(typeof(Keys), keystr);
            }
            catch (Exception ex)
            {
                UI.Notify("Error: " + ex.Message);
                return Keys.F7;
            }
        }

        private void WarehouseRobbery_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ConvertFromString(_keyMenu))
            {
                if (!playerinzone == false)
                {
                    mWarehouse.Visible = !mWarehouse.Visible;
                }
            }
            if (e.KeyCode == ConvertFromString(_keyRemoveWeapons))
            {
                Game.Player.Character.Weapons.RemoveAll();
                UI.Notify("All weapons removed");
            }
        }
    }

    public class WarehouseAmmunation
    {
        public static int Ammo
        {
            get { return warehouse.Default._wrammo; }
            set { warehouse.Default._wrammo = value; warehouse.Default.Save(); }
        }

        public static int Grenades
        {
            get { return warehouse.Default._wrgrenades; }
            set { warehouse.Default._wrgrenades = value; warehouse.Default.Save(); }
        }
    }
}