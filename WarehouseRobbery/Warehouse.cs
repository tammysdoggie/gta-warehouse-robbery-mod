﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTA;
using GTA.Math;

namespace WarehouseRobbery
{
    public class Warehouse
    {
        public Warehouse(string wrName, Vector3 pos)
        {
            Trigger = pos;
            Name = wrName;
        }

        public string Name
        {
            get { return this.Name; }
            set { this.Name = value; }
        }

        public Vector3 Trigger
        {
            get { return this.Trigger; }
            set { this.Trigger = value; }
        }
    }
}
